export interface SearchRequest {
  limit?: number;
  page?: number;
  code?: string;
}

export interface TradeRequest {
  currency: string;
  amount: number;
}
