import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Currency, CurrencyDocument } from './schemas/currency.schema';
import { SearchRequest } from './interfaces/currency.interface';

@Injectable()
export class CurrencyService {
  constructor(
    @InjectModel(Currency.name)
    private readonly currencyModel: Model<CurrencyDocument>,
  ) {}

  async search(data: SearchRequest): Promise<Currency[]> {
    const filter: { code?: string } = {};
    if (data.code) {
      filter.code = data.code;
    }

    const limit = data.limit || 10;
    const page = data.page || 1;

    return this.currencyModel
      .find(filter)
      .skip((page - 1) * limit)
      .limit(limit)
      .exec();
  }
}
