import { Controller } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';
import { CurrencyService } from './currency.service';
import { Currency } from './schemas/currency.schema';
import { SearchRequest, TradeRequest } from './interfaces/currency.interface';

@Controller()
export class CurrencyController {
  constructor(private readonly currencyService: CurrencyService) {}

  @GrpcMethod('CurrencyService', 'Search')
  async search(data: SearchRequest): Promise<{ currencies: Currency[] }> {
    return {
      currencies: await this.currencyService.search(data),
    };
  }

  @GrpcMethod('CurrencyService', 'Buy')
  async buy(data: TradeRequest): Promise<{ status: boolean; course: number }> {
    const currency = data.currency;
    const amount = data.amount; // currency in cents

    return {
      status: true,
      course: 55,
    };
  }

  @GrpcMethod('CurrencyService', 'Sell')
  async sell(data: TradeRequest): Promise<{ status: boolean; course: number }> {
    const currency = data.currency;
    const amount = data.amount; // currency in cents

    return {
      status: true,
      course: 55,
    };
  }
}
