import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { ConfigModule } from '@nestjs/config';
import { CurrencyModule } from './currency/currency.module';
import { HistoryModule } from './history/history.module';
import { env } from './libs';

@Module({
  imports: [
    ConfigModule.forRoot(),
    MongooseModule.forRoot(
      `mongodb://${env('MONGO_HOST')}:${env('MONGO_PORT')}/` +
        `${env('MONGO_DATABASE')}`,
    ),
    CurrencyModule,
    HistoryModule,
    ScheduleModule.forRoot(),
  ],
})
export class AppModule {}
