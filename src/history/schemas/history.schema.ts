import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type HistoryDocument = HydratedDocument<History>;

@Schema()
export class History {
  @Prop()
  currency: string;

  @Prop({
    get: (value: Date) => {
      return value.toLocaleDateString();
    },
  })
  date: Date;

  @Prop()
  course: number;
}

export const HistorySchema = SchemaFactory.createForClass(History);
