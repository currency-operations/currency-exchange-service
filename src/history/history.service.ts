import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { InjectModel } from '@nestjs/mongoose';
import { Model, SortOrder } from 'mongoose';
import { HttpService } from '@nestjs/axios';
import { catchError, firstValueFrom } from 'rxjs';
import { History, HistoryDocument } from './schemas/history.schema';
import { HistoryRequest } from './interfaces/history.interface';
import { CurrencyService } from '../currency/currency.service';

@Injectable()
export class HistoryService {
  constructor(
    @InjectModel(History.name)
    private readonly historyModel: Model<HistoryDocument>,

    private readonly currencyService: CurrencyService,
    private readonly httpService: HttpService,
  ) {}

  async histories(data: HistoryRequest): Promise<History[]> {
    if (!data.currency) {
      return [];
    }

    const filter: { currency: string; date?: any } = {
      currency: data.currency,
    };

    if (data.from) {
      const date = new Date(data.from);
      filter.date = {
        ...filter.date,
        $gte: `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`,
      };
    }

    if (data.to) {
      const date = new Date(data.to);
      filter.date = {
        ...filter.date,
        $lte: `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`,
      };
    }

    const limit = data.limit || 10;
    const page = data.page || 1;
    const sort: { [key: string]: SortOrder } = { date: 1 };

    return this.historyModel
      .find(filter)
      .sort(sort)
      .skip((page - 1) * limit)
      .limit(limit)
      .exec();
  }

  // @Timeout(1000)
  @Cron(CronExpression.EVERY_DAY_AT_10AM, {
    name: 'import',
    timeZone: 'Asia/Novosibirsk',
  })
  async handleImportJob() {
    console.log('Cron Job - Started');
    const currencies = await this.currencyService.search({});

    if (!currencies) {
      return;
    }

    for (const currency of currencies) {
      if (currency.code === 'RUB') {
        continue;
      }

      const start = new Date('2022-11-01');
      const end = new Date();

      for (let d = start; d < end; d.setDate(d.getDate() + 1)) {
        const date = d.toISOString().split('T')[0];

        const { data }: { data: { rub: number; date: string } } =
          await firstValueFrom(
            this.httpService
              .get(
                `https://cdn.jsdelivr.net/gh/fawazahmed0/currency-api@1/${date}/currencies/${currency.code.toLowerCase()}/rub.min.json`,
              )
              .pipe(
                catchError((error) => {
                  console.error(error.response.data);
                  throw 'An error happened!';
                }),
              ),
          );

        if (data) {
          await this.historyModel.create({
            currency: currency.code,
            course: data.rub,
            date: data.date,
          });
        }
      }
    }

    console.log('Cron Job - Ended');
  }
}
