export interface HistoryRequest {
  currency: string;
  limit?: number;
  page?: number;
  from?: string;
  to?: string;
}
