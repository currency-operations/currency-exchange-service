import { Controller } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';
import { HistoryService } from './history.service';
import { History } from './schemas/history.schema';
import { HistoryRequest } from './interfaces/history.interface';

@Controller()
export class HistoryController {
  constructor(private readonly historyService: HistoryService) {}

  @GrpcMethod('CurrencyService', 'History')
  async histories(data: HistoryRequest): Promise<{ histories: History[] }> {
    return {
      histories: await this.historyService.histories(data),
    };
  }
}
