import { ClientOptions, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { env } from './libs';

export const grpcClientOptions: ClientOptions = {
  transport: Transport.GRPC,
  options: {
    package: 'currency',
    url: `${env('GRPC_HOST')}:${env('GRPC_PORT')}`,
    protoPath: join(__dirname, '../proto/currency.proto'),
  },
};
