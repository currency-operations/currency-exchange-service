export type envType = string | number | boolean | null;

export const env = (key: string, defaultValue: envType = null): envType => {
  let value: envType | undefined = process.env[key];
  if (!value) {
    return defaultValue;
  }
  switch (value.toLowerCase()) {
    case 'true':
    case '(true)':
      value = true;
      break;
    case 'false':
    case '(false)':
      value = false;
      break;
    case 'empty':
    case '(empty)':
      value = '';
      break;
    case 'null':
    case '(null)':
      value = null;
      break;
  }
  return value;
};
